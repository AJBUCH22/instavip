import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

import { FooterService } from '../../services/components/footer.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {

  headerIcon:string = 'iman-1.png';
  quantity:number = 10.3;
  processIcon:string = 'w-search.png';
  automaticIcon: string = "users-group.png";

  constructor(private menu: MenuController,
    private router: Router,
    private footerService: FooterService) { }


  ngOnInit() {

    this.footerService.currentSection.subscribe(section => {

      if (section == 'likes') {

        this.headerIcon = 'c-like.png';
        this.processIcon = 'w-search.png';
        this.automaticIcon = "users-group.png";

      } else if (section == 'followers') {

        this.headerIcon = 'c-frends.png';
        this.processIcon = 'w-search.png';
        this.automaticIcon = "users-group.png";

      } else if (section == 'comments') {

        this.headerIcon = 'c-followers.png';
        this.processIcon = 'w-search.png';
        this.automaticIcon = "users-group.png";

      } else if (section == 'videos') {

        this.headerIcon = 'c-videos.png';
        this.processIcon = 'w-search.png';
        this.automaticIcon = "users-group.png";

      } else if (section == 'selec-automatic-mode') {

        this.headerIcon = 'iman-1.png';
        this.processIcon = 'w-search.png';
        this.automaticIcon = "c-users-group.png";

      } else if (section == 'process') {

        this.headerIcon = 'iman-1.png';
        this.processIcon = 'c-search.png';
        this.automaticIcon = "users-group.png";

      } else {

        this.headerIcon = 'iman-1.png';
        this.processIcon = 'w-search.png';
        this.automaticIcon = "users-group.png";
        
      }

    });

  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }

  goProcess() {

    let activeRoute: string = this.router.url;
    let mainRoute: string = activeRoute.split('/')[1];
    let goToRoute: string = '';

    if (mainRoute == 'likes') {

      goToRoute = '/likes/getting-likes';

    } else if (mainRoute == 'followers') {

      goToRoute = '/followers/getting-followers';

    } else if (mainRoute == 'comments') {

      goToRoute = '/comments/getting-comments';

    } else if (mainRoute == 'videos') {

      goToRoute = '/videos/getting-videos';

    } else {

      goToRoute = '/selec-automatic-mode/selec-automatic-mode';

    }

    this.router.navigate([goToRoute]);

  }

}
