import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer.component';

@NgModule({
  declarations: [FooterComponent],
  exports: [FooterComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    RouterModule
  ],
})
export class FooterModule { }
