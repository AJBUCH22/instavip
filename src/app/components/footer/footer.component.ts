import { Component, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { FooterService } from '../../services/components/footer.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {

  @ViewChild('magnetismAnimation', {static: false}) magnetismAnimation: ElementRef;

  likeIcon: string = "b-like.png";
  followersIcon: string = "b-frends.png";
  commentsIcon: string = "followers.png";
  videosIcon: string = "b-videos.png";

  constructor(private router: Router,
    private footerService: FooterService) { }

  ngOnInit() {

    this.getCurrentSection();

  }

  getCurrentSection(){

    this.footerService.currentSection.subscribe(section => { 

      if (section == 'likes') {

        this.likeIconToggle();

      } else if (section == 'followers') {

        this.followersIconToggle();

      } else if (section == 'comments') {

        this.commentsIconToggle();

      } else if (section == 'videos') {

        this.videosIconToggle();

      } else if (section == 'selec-automatic-mode') {

        this.automaticIconToggle();

      } else if (section == 'other' || section == 'process') {

        this.magnetismIconToggle();

      }

    });

  }


  commentsIconToggle() {

    this.likeIcon = "b-like.png";
    this.followersIcon = "b-frends.png";
    this.commentsIcon = "c-followers.png";
    this.videosIcon = "b-videos.png";

  }

  followersIconToggle() {

    this.likeIcon = "b-like.png";
    this.followersIcon = "c-frends.png";
    this.commentsIcon = "followers.png";
    this.videosIcon = "b-videos.png";

  }

  likeIconToggle() {

    this.likeIcon = "c-like.png";
    this.followersIcon = "b-frends.png";
    this.commentsIcon = "followers.png";
    this.videosIcon = "b-videos.png";

  }

  videosIconToggle() {

    this.likeIcon = "b-like.png";
    this.followersIcon = "b-frends.png";
    this.commentsIcon = "followers.png";
    this.videosIcon = "c-videos.png";

  }

  automaticIconToggle() {

    this.likeIcon = "b-like.png";
    this.followersIcon = "b-frends.png";
    this.commentsIcon = "followers.png";
    this.videosIcon = "b-videos.png";

  }

  magnetismIconToggle() {

    this.likeIcon = "b-like.png";
    this.followersIcon = "b-frends.png";
    this.commentsIcon = "followers.png";
    this.videosIcon = "b-videos.png";

  }

  playMagnetismAnimation(){

    this.magnetismAnimation.nativeElement.play();

    setTimeout(() => {

      this.magnetismIconToggle();
      this.router.navigate(['/magnetism/magnetism']);

     }, 2000);
    
  }

}
