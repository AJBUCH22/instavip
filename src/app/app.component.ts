import { Component } from '@angular/core';

import { MenuController } from '@ionic/angular';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AutenticationService } from './services/autentication/autentication.service'

// Import Auth0Cordova
import Auth0Cordova from '@auth0/cordova';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  showHeader: boolean = false;
  showFooter: boolean = false;

  constructor(
    private menu: MenuController,
    private platform: Platform,
    private router: Router,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AutenticationService,
  ) {
    this.initializeApp();
  }

  closeMenu(){

    this.menu.close('first');

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      // Redirect back to app after authenticating
      (window as any).handleOpenURL = (url: string) => {
        Auth0Cordova.onRedirectUri(url);
      }

      // this.router.navigate(['videos/getting-videos']);
      //     this.showHeader = true;
      //     this.showFooter = true;

      this.authService.autenticationState.subscribe(state => {

        if (state) {
          this.router.navigate(['likes/likes-manual']);
          this.showHeader = true;
          this.showFooter = true;
        } else {
          this.router.navigate(['login']);
        }

      });

    });

  }
}
