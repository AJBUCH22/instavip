import { Injectable } from '@angular/core';

export const AgeList:Array<any> = [
  {
    value: 16,
  },
  {
    value: 17,
  },
  {
    value: 18,
  },
  {
    value: 19,
  },
  {
    value: 20,
  },
  {
    value: 21,
  },
];

export const GenderList:Array<any> = [
  {
    value: 1,
    name: 'Femenino'
  },
  {
    value: 2,
    name: 'Masculino'
  }
];

export const ThematicsList:Array<any> = [
  {
    value: 1,
    name: 'Comida'
  },
  {
    value: 2,
    name: 'Cuidado Personal'
  },
  {
    value: 3,
    name: 'Deportes'
  },
  {
    value: 4,
    name: 'Viajes'
  }
];

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor() { }
}
