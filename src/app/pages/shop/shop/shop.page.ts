import { Component, OnInit } from '@angular/core';

import { FooterService } from '../../../services/components/footer.service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.page.html',
  styleUrls: ['./shop.page.scss'],
})
export class ShopPage implements OnInit {

  likeIcon:string = "w-like.png";
  followersIcon:string = "w-frends.png";
  commentsIcon:string = "w-followers.png";
  videosIcon:string = "w-videos.png";

  likesDiv:boolean = false;
  followersDiv:boolean = false;
  commentsDiv:boolean = false;
  videosDiv:boolean = false;
  showButton:boolean = false;

  constructor(private footerService: FooterService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {

    this.footerService.getCurrentSection();

  }

  commentsIconToggle(){

    this.showButton = false;

    if( this.commentsIcon == "w-followers.png" ){

      this.commentsIcon = "c-followers.png";
      this.commentsDiv = true;
      this.showButton = true;

    }else{

      this.commentsIcon = "w-followers.png";
      this.commentsDiv = false;

    }

    this.followersIcon = "w-frends.png";
    this.likeIcon = "w-like.png";
    this.videosIcon == "w-videos.png";
    this.followersDiv = false;
    this.likesDiv = false;
    this.videosDiv = false;

  }

  followersIconToggle(){

    this.showButton = false;

    if( this.followersIcon == "w-frends.png" ){

      this.followersIcon = "c-frends.png";
      this.followersDiv = true;
      this.showButton = true;

    }else{

      this.followersIcon = "w-frends.png";
      this.followersDiv = false;

    }

    this.commentsIcon = "w-followers.png";
    this.likeIcon = "w-like.png";
    this.videosIcon == "w-videos.png";
    this.commentsDiv = false;
    this.likesDiv = false;
    this.videosDiv = false;

  }

  likeIconToggle(){

    this.showButton = false;

    if( this.likeIcon == "w-like.png" ){

      this.likeIcon = "c-like.png";
      this.likesDiv = true;
      this.showButton = true;

    }else{

      this.likeIcon = "w-like.png";
      this.likesDiv = false;

    }

    this.commentsIcon = "w-followers.png";
    this.followersIcon = "w-frends.png";
    this.videosIcon == "w-videos.png";
    this.commentsDiv = false;
    this.followersDiv = false;
    this.videosDiv = false;

  }

  videosIconToggle(){

    this.showButton = false;

    if( this.videosIcon == "w-videos.png" ){

      this.videosIcon = "c-videos.png";
      this.videosDiv = true;
      this.showButton = true;

    }else{

      this.videosIcon = "w-videos.png";
      this.videosDiv = false;

    }

    this.commentsIcon = "w-followers.png";
    this.followersIcon = "w-frends.png";
    this.likeIcon = "w-like.png";
    this.commentsDiv = false;
    this.followersDiv = false;
    this.likesDiv = false;

  }

}
