import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { FooterService } from '../../../services/components/footer.service';

@Component({
  selector: 'app-select-magnetism',
  templateUrl: './select-magnetism.page.html',
  styleUrls: ['./select-magnetism.page.scss'],
})
export class SelectMagnetismPage implements OnInit {

  likeIcon:string = "w-like.png";
  followersIcon:string = "w-frends.png";
  commentsIcon:string = "w-followers.png";
  videosIcon:string = "w-videos.png";

  constructor(public modalController: ModalController,
    private footerService: FooterService) { }

  ngOnInit() {}

  ionViewDidEnter() {

    this.footerService.getCurrentSection();

  }

  closeModal(){

    this.modalController.dismiss({
      'dismissed': true
    });

  }

  commentsIconToggle(){

    if( this.commentsIcon == "w-followers.png" ){

      this.commentsIcon = "c-followers.png";

    }else{

      this.commentsIcon = "w-followers.png";

    }

    this.followersIcon = "w-frends.png";
    this.likeIcon = "w-like.png";
    this.videosIcon == "w-videos.png"

  }

  followersIconToggle(){

    if( this.followersIcon == "w-frends.png" ){

      this.followersIcon = "c-frends.png";

    }else{

      this.followersIcon = "w-frends.png";

    }

    this.commentsIcon = "w-followers.png";
    this.likeIcon = "w-like.png";
    this.videosIcon == "w-videos.png"

  }

  likeIconToggle(){

    if( this.likeIcon == "w-like.png" ){

      this.likeIcon = "c-like.png";

    }else{

      this.likeIcon = "w-like.png";

    }

    this.commentsIcon = "w-followers.png";
    this.followersIcon = "w-frends.png";
    this.videosIcon == "w-videos.png";

  }

  videosIconToggle(){

    if( this.videosIcon == "w-videos.png" ){

      this.videosIcon = "c-videos.png";

    }else{

      this.videosIcon = "w-videos.png";

    }

    this.commentsIcon = "w-followers.png";
    this.followersIcon = "w-frends.png";
    this.followersIcon = "w-like.png";

  }

}
