import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectMagnetismMessageModalPage } from './select-magnetism-message-modal.page';

const routes: Routes = [
  {
    path: '',
    component: SelectMagnetismMessageModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectMagnetismMessageModalPageRoutingModule {}
