import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { FooterService } from '../../../services/components/footer.service';

@Component({
  selector: 'app-select-magnetism-message-modal',
  templateUrl: './select-magnetism-message-modal.page.html',
  styleUrls: ['./select-magnetism-message-modal.page.scss'],
})
export class SelectMagnetismMessageModalPage implements OnInit {

  constructor(public modalController: ModalController,
    private footerService: FooterService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {

    this.footerService.getCurrentSection();

  }

  closeModal(){

    this.modalController.dismiss({
      'dismissed': true
    });

  }

}
