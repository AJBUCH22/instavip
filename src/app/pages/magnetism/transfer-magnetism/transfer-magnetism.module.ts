import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TransferMagnetismPage } from './transfer-magnetism.page';
import { TransferMagnetismModalPage } from '../transfer-magnetism-modal/transfer-magnetism-modal.page';

const routes: Routes = [
  {
    path: '',
    component: TransferMagnetismPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TransferMagnetismPage, TransferMagnetismModalPage],
  entryComponents: [TransferMagnetismModalPage]
})
export class TransferMagnetismPageModule {}
