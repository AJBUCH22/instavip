import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ExchangeMagnetismPage } from '../exchange-magnetism/exchange-magnetism.page';
import { SelectMagnetismPage } from '../select-magnetism/select-magnetism.page';
import { SelectMagnetismMessageModalPage } from '../select-magnetism-message-modal/select-magnetism-message-modal.page';

import { FooterService } from '../../../services/components/footer.service';

@Component({
  selector: 'app-magnetism',
  templateUrl: './magnetism.page.html',
  styleUrls: ['./magnetism.page.scss'],
})
export class MagnetismPage implements OnInit {

  @ViewChild('magnetismAnimation', {static: false}) magnetismAnimation: ElementRef;

  followers:number = 55;
  userId:string = '0011';

  constructor(public modalController: ModalController,
    private footerService: FooterService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {

    this.footerService.getCurrentSection();

  }

  playMagnetismAnimation(){

    this.magnetismAnimation.nativeElement.play();

    setTimeout(() => {

      this.presentSelectMagnetismModal();

     }, 2000);
    
  }

  async presentSelectMagnetismModal() {
    const modal = await this.modalController.create({
      component: SelectMagnetismPage
    });
    modal.onWillDismiss().then(() => this.presentSelectMagnetismMessageModal() );
    return await modal.present();
  }

  async presentSelectMagnetismMessageModal() {
    const modal = await this.modalController.create({
      component: SelectMagnetismMessageModalPage
    });
    return await modal.present();
  }

  async presentExchangeMagnetismModal() {
    const modal = await this.modalController.create({
      component: ExchangeMagnetismPage,
      componentProps: {
        'followers': this.followers
      }
    });
    return await modal.present();
  }

}
