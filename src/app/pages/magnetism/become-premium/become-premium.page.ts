import { Component, OnInit } from '@angular/core';

import { FooterService } from '../../../services/components/footer.service';

@Component({
  selector: 'app-become-premium',
  templateUrl: './become-premium.page.html',
  styleUrls: ['./become-premium.page.scss'],
})
export class BecomePremiumPage implements OnInit {

  constructor(private footerService: FooterService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {

    this.footerService.getCurrentSection();

  }

}
