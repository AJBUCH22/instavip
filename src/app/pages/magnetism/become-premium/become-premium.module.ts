import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BecomePremiumPage } from './become-premium.page';

const routes: Routes = [
  {
    path: '',
    component: BecomePremiumPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [BecomePremiumPage]
})
export class BecomePremiumPageModule {}
