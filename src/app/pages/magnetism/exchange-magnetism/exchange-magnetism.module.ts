import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ExchangeMagnetismPage } from './exchange-magnetism.page';

const routes: Routes = [
  {
    path: '',
    component: ExchangeMagnetismPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  // declarations: [ExchangeMagnetismPage]
})
export class ExchangeMagnetismPageModule {}
