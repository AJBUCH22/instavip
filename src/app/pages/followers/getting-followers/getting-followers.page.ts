import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { FooterService } from '../../../services/components/footer.service';

@Component({
  selector: 'app-getting-followers',
  templateUrl: './getting-followers.page.html',
  styleUrls: ['./getting-followers.page.scss'],
})
export class GettingFollowersPage implements OnInit {

  avatar: string = 'assets/imgs/samples/avatar-sample1.jpg';
  form01: FormGroup;
  name: string = 'Araceli González';
  text2: string = '218';
  text3: string = '3K1';

  constructor(private _formBuilder: FormBuilder,
    private router: Router,
    private footerService: FooterService) {}

  ngOnInit() {

    this.form01 = this._formBuilder.group({
      url: ['', Validators.required]
    });
    
  }

  ionViewDidEnter() {

    this.footerService.getCurrentSection();

  }

  onSubmit() {

  }

}
