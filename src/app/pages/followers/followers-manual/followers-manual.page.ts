import { Component, OnInit } from '@angular/core';

import { UsersList } from '../../../services/likes/likes.service';
import { FooterService } from '../../../services/components/footer.service';

@Component({
  selector: 'app-followers-manual',
  templateUrl: './followers-manual.page.html',
  styleUrls: ['./followers-manual.page.scss'],
})
export class FollowersManualPage implements OnInit {

  modalAvatar:string;
  modalDiv:boolean = false;
  modalName:string;
  modalQuantity:number = 0.3;
  usersList:Array<any> = UsersList;

  constructor(private footerService: FooterService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {

    this.footerService.getCurrentSection();

  }

  showModal( index:number ){

    this.modalAvatar = this.usersList[index].avatar;
    this.modalName = this.usersList[index].name + ' ' + this.usersList[index].lastName;
    this.modalDiv = true;

  }

  closeModal(){

    this.modalDiv = false;

  }

}
