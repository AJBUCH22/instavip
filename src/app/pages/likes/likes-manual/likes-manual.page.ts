import { Component, OnInit } from '@angular/core';

import { PublicationList } from '../../../services/likes/likes.service';
import { FooterService } from '../../../services/components/footer.service';

@Component({
  selector: 'app-likes-manual',
  templateUrl: './likes-manual.page.html',
  styleUrls: ['./likes-manual.page.scss'],
})
export class LikesManualPage implements OnInit {

  modalPicture:string;
  modalDiv:boolean = false;
  modalName:string;
  modalQuantity:number = 0.3;
  publicationList:Array<any> = PublicationList;

  constructor(private footerService: FooterService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {

    this.footerService.getCurrentSection();

  }

  showModal( index:number ){

    this.modalPicture = this.publicationList[index].picture;
    this.modalName = this.publicationList[index].name;
    this.modalDiv = true;

  }

  closeModal(){

    this.modalDiv = false;

  }

}
