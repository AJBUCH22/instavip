import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../../services/instagram-auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  providers: [AuthService]
})
export class LoginPage implements OnInit {

  @ViewChild('loginAnimation', {static: false}) loginAnimation: ElementRef;

  showVideo: boolean = true;

  constructor(public auth: AuthService,
    private router: Router, ) { }

  ngOnInit() {

    setTimeout(() => {

      this.showVideo = false;

     }, 10000);

  }

  goToStart() {
    // this.auth.login();
    // localStorage.setItem('instavipUser', JSON.stringify(this.auth.user));
    // alert(JSON.stringify(this.auth.user));
    this.router.navigate(['forms/form01']);
  }

}
