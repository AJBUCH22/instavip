import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VideosManualPageRoutingModule } from './videos-manual-routing.module';

import { VideosManualPage } from './videos-manual.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VideosManualPageRoutingModule
  ],
  declarations: [VideosManualPage]
})
export class VideosManualPageModule {}
