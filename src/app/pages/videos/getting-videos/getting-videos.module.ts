import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GettingVideosPageRoutingModule } from './getting-videos-routing.module';

import { GettingVideosPage } from './getting-videos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    GettingVideosPageRoutingModule
  ],
  declarations: [GettingVideosPage]
})
export class GettingVideosPageModule {}
