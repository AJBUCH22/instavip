import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { FooterService } from '../../../services/components/footer.service';

@Component({
  selector: 'app-getting-videos',
  templateUrl: './getting-videos.page.html',
  styleUrls: ['./getting-videos.page.scss'],
})
export class GettingVideosPage implements OnInit {

  avatar: string = 'assets/imgs/samples/avatar-sample1.jpg';
  form01: FormGroup;
  name: string = 'Araceli González';
  text2: string = '218';
  text3: string = '3K1';

  constructor(private _formBuilder: FormBuilder,
    private router: Router,
    private footerService: FooterService) {}

  ngOnInit() {

    this.form01 = this._formBuilder.group({
      url: ['', Validators.required],
      progress: ['', Validators.required]
    });
    
  }

  ionViewDidEnter() {

    this.footerService.getCurrentSection();

  }

  onSubmit() {

  }

}
