import { Component, OnInit } from '@angular/core';

import { FooterService } from '../../../services/components/footer.service';

@Component({
  selector: 'app-process',
  templateUrl: './process.page.html',
  styleUrls: ['./process.page.scss'],
})
export class ProcessPage implements OnInit {

  avatar:string = 'assets/imgs/samples/avatar-sample1.jpg';
  name:string = 'Araceli González';
  text2:string = '218';
  text3:string = '3K1';
  text4:string = '1k2';

  modalDiv:boolean = false;

  constructor(private footerService: FooterService) { }

  ngOnInit() {}

  ionViewDidEnter() {

    this.footerService.getCurrentSection();

  }

  showModal(){

    this.modalDiv = true;

  }

  closeModal(){

    this.modalDiv = false;

  }

}
