import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SelecAutomaticModePage } from './selec-automatic-mode.page';

const routes: Routes = [
  {
    path: '',
    component: SelecAutomaticModePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [SelecAutomaticModePage]
})
export class SelecAutomaticModePageModule {}
