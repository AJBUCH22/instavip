import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AutenticationService } from '../../../services/autentication/autentication.service'

@Component({
  selector: 'app-analyzing02',
  templateUrl: './analyzing02.page.html',
  styleUrls: ['./analyzing02.page.scss'],
})
export class Analyzing02Page implements OnInit {

  form01: FormGroup;
  avatar: string = 'assets/imgs/samples/avatar-sample1.jpg';
  name: string = 'Araceli González';
  text1: string = 'CUMPLES CON LOS REQUISITOS';
  text2: string = 'PUEDES EMPEZAR A USAR LA APP';
  userId: string = '0011';

  constructor(private _formBuilder: FormBuilder,
    private authService: AutenticationService, ) { }

  ngOnInit() {

    this.form01 = this._formBuilder.group({
      conditions: [false, Validators.required]
    });

  }

  onSubmit() {

    this.authService.login();

  }

  conditionsToggle(){

    if( this.form01.get('conditions').value ){
      this.form01.get('conditions').setValue(false);
    }else{
      this.form01.get('conditions').setValue(true);
    }

  }

}
