import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-analyzing01',
  templateUrl: './analyzing01.page.html',
  styleUrls: ['./analyzing01.page.scss'],
})
export class Analyzing01Page implements OnInit {

  avatar: string = 'assets/imgs/samples/avatar-sample1.jpg';
  name: string = 'Araceli González';
  form01: FormGroup;
  message: string = 'Estamos analizando tu cuenta. Sólo nos llevará un momento!!';
  processStep: number = 0;
  percentage: number = 0;
  step1: boolean = false;
  step2: boolean = false;
  step3: boolean = false;
  step4: boolean = false;
  step5: boolean = false;
  step6: boolean = false;

  constructor(private _formBuilder: FormBuilder,
    private router: Router) { }

  ngOnInit() {

    this.form01 = this._formBuilder.group({
      photo: ['', Validators.required],
      folowers: ['', Validators.required],
      posts: ['', Validators.required],
      publication: ['', Validators.required],
      more_folowers: ['', Validators.required],
      email: ['', Validators.required]
    });

    this.analizingProcess();

  }

  analizingProcess() {

    setTimeout(() => {

      this.processAnimation();

    }, 1000);

  }

  processAnimation() {

    this.processStep++;

    if (this.processStep == 1) {

      this.message = 'Vemos que tienes un avatar, se te ve muy bien!!';
      this.percentage += 20;
      this.step1 = true;
      this.analizingProcess();

    } else if (this.processStep == 2) {

      this.message = 'Tienes bastantes seguidores, bien!!';
      this.percentage += 20;
      this.step2 = true;
      this.analizingProcess();

    } else if (this.processStep == 3) {

      this.message = 'Tu última publicación es bastante reciente. Es importante que tu cuenta este viva!!';
      this.percentage += 20;
      this.step3 = true;
      this.analizingProcess();

    } else if (this.processStep == 4) {

      this.message = 'El equilibrio entre seguidores y seguidos es el adecuado, en la vida todo es equilibrio!!';
      this.percentage += 20;
      this.step4 = true;
      this.analizingProcess();

    } else if (this.processStep == 5) {

      this.message = 'Tienes bastantes seguidores, bien!!';
      this.percentage += 20;
      this.step5 = true;
      this.analizingProcess();

    } else if (this.processStep == 6) {

      this.message = 'Generando ID de usuario';
      this.step6 = true;
      this.analizingProcess();

    } else if (this.processStep == 7) {

      this.router.navigate(['/forms/analyzing02']);

    }


  }

}
