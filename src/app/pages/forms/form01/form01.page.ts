import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AgeList, GenderList, ThematicsList } from '../../../services/forms/form.service';
import { ProvincesService } from '../../../services/provinces/provinces.service';

@Component({
  selector: 'app-form01',
  templateUrl: './form01.page.html',
  styleUrls: ['./form01.page.scss'],
})
export class Form01Page implements OnInit {

  avatar:string = 'assets/imgs/samples/avatar-sample1.jpg';
  data:any;
  name:string = 'Araceli González';
  text1:string = 'Para poder ofrecerte una mejor calidad de nuetro servicio, necesitamos hacerte unas preguntas!!'; 
  form01: FormGroup;
  ageList:Array<any> = AgeList;
  genderList:Array<any> = GenderList;
  thematicsList:Array<any> = ThematicsList;
  provincesList:Array<any>;

  constructor(private _formBuilder: FormBuilder,
              private router: Router,
              private provincesService: ProvincesService) {}

  ngOnInit() {

    this.form01 = this._formBuilder.group({
      age: ['', Validators.required],
      gender: ['', Validators.required],
      province: ['', Validators.required],
      thematic: ['', Validators.required],
      promotions: ['', Validators.required],
      email: ['', Validators.required]
    });

    this.getProvinces();
    
  }

  getProvinces(){

    this.data = [];
  
    this.provincesService.getProvinces()
      .then(data => {
        this.data = data;        
        this.provincesList = this.data;
      },
      error => {
        // console.error(error);
      });

  }

  onSubmit(){

    this.router.navigate(['/forms/analyzing01']);

  }

  promotionsToggle(){

    if( this.form01.get('promotions').value ){
      this.form01.get('promotions').setValue(false);
    }else{
      this.form01.get('promotions').setValue(true);
    }

  }

}
